import java.util.*;
import java.io.*;
import java.math.*;

class Player {

    public static void main(String args[]) {
        Scanner in = new Scanner(System.in);

        // game loop
        while (true) {
            int hightestMountain = Integer.MIN_VALUE;
            int index = 0;
            for (int i = 0; i < 8; i++) {
                int mountainH = in.nextInt(); // represents the height of one mountain.
                if (mountainH > hightestMountain && mountainH >= 0 && mountainH <= 9) {
                    hightestMountain = mountainH;
                    index = i;
                }
            }

            System.out.println(index); // The index of the mountain to fire on.
        }
    }
}