import java.util.*;
import java.io.*;
import java.math.*;

class Solution {

    public static void main(String args[]) {
        Scanner in = new Scanner(System.in);
        String MESSAGE = in.nextLine();
        int step = 0;
        char firstBin = ' ';
        for (int j = 0; j < MESSAGE.length(); j++) {
            String binary = String.format("%7s", Integer.toBinaryString(MESSAGE.charAt(j))).replace(' ', '0');
            if (step == 0) {
                firstBin = binary.charAt(0);
                if (firstBin == '1') {
                    System.out.print("0 ");
                } else {
                    System.out.print("00 ");
                }
            }

            for (int i = 0; i < binary.length(); i++) {
                if (firstBin == binary.charAt(i)) {
                    System.out.print("0");
                } else {
                    firstBin = binary.charAt(i);
                    if (firstBin == '1') {
                        System.out.print(" 0 0");
                    } else {
                        System.out.print(" 00 0");
                    }
                }
            }
            step++;
        }

    }
}