import java.util.*;
import java.io.*;
import java.math.*;

class Solution {

    public static void main(String args[]) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt(); // the number of temperatures to analyse
        int minTemp = Integer.MAX_VALUE;
        if (n == 0) {
            minTemp = 0;
        } else {
            for (int i = 0; i < n; i++) {
                int t = in.nextInt(); // a temperature expressed as an integer ranging from -273 to 5526
                if (Math.abs(t) < Math.abs(minTemp)) {
                    minTemp = t;
                } else if (Math.abs(t) == Math.abs(minTemp)) {
                    minTemp = Math.max(t, minTemp);
                }
            }
        }

        System.out.println(minTemp);
    }
}