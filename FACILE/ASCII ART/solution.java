import java.util.*;
import java.io.*;
import java.math.*;

class Solution {

    public static void main(String args[]) {
        Scanner in = new Scanner(System.in);
        int L = in.nextInt();
        int H = in.nextInt();
        if (in.hasNextLine()) {
            in.nextLine();
        }
        String T = in.nextLine();
        char[] alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();
        int[] alphabetIndexMap = new int[26];
        for (int i = 0; i < alphabet.length; i++) {
            alphabetIndexMap[i] = i;
        }
        ArrayList<Integer> wordIndex = new ArrayList();
        for (int wordLength = 0; wordLength < T.length(); wordLength++) {
            int index = getAlphabetIndex(T.charAt(wordLength), alphabet, alphabetIndexMap);
            wordIndex.add(index);
        }
        for (int i = 0; i < H; i++) {
            String ROW = in.nextLine();
            for (int wordLength = 0; wordLength < T.length(); wordLength++) {
                for (int width = 0; width < L; width++) {
                    System.out.print(ROW.charAt(L * wordIndex.get(wordLength) + width));
                }
            }
            System.out.println();

        }

    }

    public static int getAlphabetIndex(char character, char[] alphabet, int[] alphabetIndexMap) {
        // Find the index of the character in the alphabet
        for (int i = 0; i < alphabet.length; i++) {
            if (alphabet[i] == Character.toUpperCase(character)) {
                return alphabetIndexMap[i];
            }
        }

        return 26;
    }
}