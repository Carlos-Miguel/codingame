import java.util.*;
import java.io.*;
import java.math.*;

class Solution {

    public static void main(String args[]) {
        int[][] grid = new int[9][9];
        Scanner in = new Scanner(System.in);
        boolean isSolved = true;

        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                int n = in.nextInt();
                grid[i][j] = n;
            }
        }

        for (int i = 0; i < 9; i++) {
            if (!isRowValid(grid, i) || !isColValid(grid, i) || !isSubgridValid(grid, i)) {
                isSolved = false;
            }
        }

        System.out.println(isSolved);
    }

    public static boolean isRowValid(int[][] grid, int row) {
        boolean[] isValid = new boolean[9];
        for (int i = 0; i < 9; i++) {
            int value = grid[row][i] - 1;
            if (isValid[value]) {
                return false;
            } else {
                isValid[value] = true;
            }
        }
        return true;
    }

    public static boolean isColValid(int[][] grid, int col) {
        boolean[] isValid = new boolean[9];
        for (int i = 0; i < 9; i++) {
            int value = grid[i][col] - 1;
            if (isValid[value]) {
                return false;
            } else {
                isValid[value] = true;
            }
        }
        return true;
    }

    public static boolean isSubgridValid(int[][] grid, int sub) {
        boolean[] isValid = new boolean[9];
        int rowOffset = (sub / 3) * 3;
        int colOffset = (sub % 3) * 3;
        for (int i = rowOffset; i < rowOffset + 3; i++) {
            for (int j = colOffset; j < colOffset + 3; j++) {
                int value = grid[i][j] - 1;
                if (isValid[value]) {
                    return false;
                } else {
                    isValid[value] = true;
                }
            }

        }
        return true;
    }
}