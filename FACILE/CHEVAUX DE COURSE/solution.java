import java.util.*;
import java.io.*;
import java.math.*;

class Solution {

    public static void main(String args[]) {
        Scanner in = new Scanner(System.in);
        int N = in.nextInt();
        ArrayList<Integer> allVal = new ArrayList<>();
        for (int i = 0; i < N; i++) {
            int pi = in.nextInt();
            allVal.add(pi);
        }
        int minDiff = Integer.MAX_VALUE;
        Collections.sort(allVal);
        for (int i = 0; i < N - 1; i++) {
            if (allVal.get(i + 1) - allVal.get(i) < minDiff) {
                minDiff = allVal.get(i + 1) - allVal.get(i);
            }
        }
        System.out.println(minDiff);
    }
}